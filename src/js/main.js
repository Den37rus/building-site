import { heroSlider } from './sliders/hero';
import { projectsSlider } from './sliders/projects';
import { reviewSlider } from './sliders/review';

import player from './homePlayer';

const fedbackForm = document.querySelector('.feedback-form');
fedbackForm.addEventListener('submit', (e) => {
  e.preventDefault();
});

const headerNav = document.querySelector('.header-nav');

const toggleMenu = () => {
  headerNav.classList.toggle('header-nav--active');
};

document.querySelector('.burger-btn').addEventListener('click', toggleMenu);

const btnScrollToTop = document.querySelector('.footer-bottom__btn');
btnScrollToTop.addEventListener('click', () => {
  window.scroll({ top: 0, behavior: 'smooth' });
});
