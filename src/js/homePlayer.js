const playerControls = document.querySelector('.control');
const player = document.querySelector('.video__player');
const playBtn = document.querySelector('.control__btn--play');
const muteBtn = document.querySelector('.control__btn--mute');
const fullscreenBtn = document.querySelector('.control__btn--fullscreen');

player.addEventListener('canplaythrough', () => {
  playerControls.classList.remove('hidden');
});

playerControls.addEventListener('mouseenter', (e) => {
  if (player.currentTime !== 0) {
    playerControls.classList.add('overlay');
  }
});

playerControls.addEventListener('mouseleave', (e) => {
  if (!player.paused) {
    playerControls.classList.remove('overlay');
    playBtn.classList.add('hidden');
    muteBtn.classList.add('hidden');
  }
});

playBtn.addEventListener('click', () => {
  if (player.paused) {
    playerControls.classList.add('overlay');
    player.play();
  } else {
    player.pause();
  }
  playBtn.classList.toggle('control__btn--pause', !player.paused);
});

muteBtn.addEventListener('click', () => {
  player.muted = !player.muted;
  muteBtn.classList.toggle('control__btn--muted', player.muted);
});

function openFullscreen() {
  if (player.requestFullscreen) {
    player.requestFullscreen();
  } else if (player.webkitRequestFullscreen) {
    player.webkitRequestFullscreen();
  } else if (player.msRequestFullscreen) {
    player.msRequestFullscreen();
  } else {
    alert('Ваше устройство не поддерживает полноэкранное видео.');
  }
}

fullscreenBtn.addEventListener('click', openFullscreen);

window.onbeforeunload = () => {
  fullscreenBtn.removeEventListener('click', openFullscreen);
};
