import Swiper, { Navigation } from 'swiper';

export const projectsSlider = new Swiper('.projects-slider', {
  slidesPerView: 3,
  slidesPerGroup: 3,
  spaceBetween: 30,
  allowTouchMove: false,
  navigation: {
    prevEl: '.projects-slider-nav__btn--prev',
    nextEl: '.projects-slider-nav__btn--next',
    disabledClass: 'projects-slider-nav__btn--disabled',
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 20,
    },
    577: {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 30,
      allowTouchMove: true,
    },
    1025: {
      slidesPerView: 3,
      slidesPerGroup: 3,
      spaceBetween: 30,
    },
  },

  modules: [Navigation],
});
