import Swiper, { Navigation, EffectCreative } from 'swiper';

export const reviewSlider = new Swiper('.review-slider', {
  slidePerView: 1,
  loop: true,
  navigation: {
    prevEl: '.review-slider-nav__btn--prev',
    nextEl: '.review-slider-nav__btn--next',
  },
  effect: 'creative',
  creativeEffect: {
    prev: {
      translate: [0, 0, -400],
      opacity: 0,
    },
    next: {
      opacity: 0,
      translate: [0, 0, 400],
    },
  },
  allowTouchMove: false,

  modules: [Navigation, EffectCreative],
});
