import Swiper, { Autoplay, Navigation, Pagination } from 'swiper';

export const heroSlider = new Swiper('.hero__slider', {
  slidePerView: 1,
  allowTouchMove: false,
  navigation: {
    nextEl: '.slider-btn--next',
    prevEl: '.slider-btn--prev',
    disabledClass: 'slider-btn--disabled',
  },
  pagination: {
    el: '.hero-pagination',
    type: 'bullets',

    bulletElement: 'div',
    bulletClass: 'hero-pagination__bullet',
    bulletActiveClass: 'hero-pagination__bullet--active',
    clickable: true,
  },
  autoplay: {
    delay: 4000,
    disableOnInteraction: false,
    pauseOnMouseEnter: true,
  },
  modules: [Navigation, Pagination, Autoplay],
});

heroSlider.autoplay.start();
